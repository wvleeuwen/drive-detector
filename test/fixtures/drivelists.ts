import { IDrive } from '../../src/drivedetector';

export const darwinList: Array<IDrive> = [
  {
    enumerator: 'diskutil',
    busType: 'SATA',
    busVersion: null,
    device: '/dev/disk0',
    devicePath: 'IODeviceTree:/PCI0@0/SATA@1F,2/PRT0@0/PMP@0',
    raw: '/dev/rdisk0',
    description: 'Samsung SSD 850 EVO 500GB Media',
    error: null,
    size: 500107862016,
    blockSize: 512,
    logicalBlockSize: 512,
    mountpoints: [],
    isReadOnly: false,
    isSystem: true,
    isVirtual: false,
    isRemovable: false,
    isCard: null,
    isSCSI: true,
    isUSB: false,
    isUAS: null 
  },
  {
    enumerator: 'diskutil',
    busType: 'SATA',
    busVersion: null,
    device: '/dev/disk1',
    devicePath: 'IODeviceTree:/PCI0@0/SATA@1F,2/PRT0@0/PMP@0',
    raw: '/dev/rdisk1',
    description: 'AppleAPFSMedia',
    error: null,
    size: 499898105856,
    blockSize: 4096,
    logicalBlockSize: 4096,
    mountpoints: [
      { path: '/', label: 'Macintosh HD' },
      { path: '/private/var/vm', label: 'VM' }
    ],
    isReadOnly: false,
    isSystem: true,
    isVirtual: true,
    isRemovable: false,
    isCard: null,
    isSCSI: true,
    isUSB: false,
    isUAS: null 
  }
];

export const darwinUsb: IDrive = { 
  enumerator: 'diskutil',
  busType: 'USB',
  busVersion: null,
  device: '/dev/disk2',
  devicePath: 'IODeviceTree:/PCI0@0/EHC2@1A',
  raw: '/dev/rdisk2',
  description: 'Sony Storage Media Media',
  error: null,
  size: 16039018496,
  blockSize: 512,
  logicalBlockSize: 512,
  mountpoints: [
    { path: '/Volumes/Untitled', label: undefined }
  ],
  isReadOnly: false,
  isSystem: false,
  isVirtual: false,
  isRemovable: true,
  isCard: null,
  isSCSI: false,
  isUSB: true,
  isUAS: null 
};