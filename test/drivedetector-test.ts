import * as os from 'os';
import { darwinList, darwinUsb } from './fixtures/drivelists';
import DriveDetector from '../src/drivedetector';
import * as Sinon from 'sinon';
import { expect } from 'chai';
import * as DriveList from 'drivelist';

const intervalTime = 1;

describe("DriveDetector class", () => {
  let driveDetector;
  let drivelistStub;
  beforeEach(() => {
    driveDetector = new DriveDetector();
    drivelistStub = Sinon.stub(DriveList, 'list');
  })

  afterEach(() => {
    driveDetector.removeAllListeners();
    drivelistStub.restore();
  })

  it("should get the complete list of mounted drives", (done) => {
    const darwinCompleteList = [...darwinList, darwinUsb]
    drivelistStub.yields(null, darwinCompleteList)

    driveDetector.list()
      .then(drives => {
        expect(drives).to.deep.equal(darwinCompleteList);
        done();
      })
      .catch(done)
  })

  it("should emit an 'error' event when an error occured in drivelist", (done) => {
    drivelistStub.yields(new Error("some error"));

    driveDetector
      .on('error', error => {
        expect(error).to.be.an('Error');
        
        driveDetector.stop();
        done();
      })
      .start(intervalTime);
  })

  it("should emit a 'started' event with the current drives", (done) => {
    drivelistStub.yields(null, darwinList);
    
    driveDetector
      .on('started', (drives) => {
        expect(drives).to.deep.equal(darwinList);
        
        driveDetector.stop();
        done();
      })
      .on('error', done)
      .start(intervalTime);
  })

  it("should emit a 'started' event when the detector was stopped and started again")

  it("should emit a 'mounted' event when a drive is mounted", (done) => {
    drivelistStub.onCall(0).callsArgWith(0, null, darwinList);
    drivelistStub.onCall(1).callsArgWith(0, null, [...darwinList, darwinUsb]);

    driveDetector
      .on('mounted', (drive) => {
        expect(drive).to.deep.equal(darwinUsb);
        
        driveDetector.stop();
        done();
      })
      .on('error', done)
      .start(intervalTime);
  })

  it("should emit a 'mounted' event when a drive is mounted and another unmounted", (done) => {
    drivelistStub.onCall(0).callsArgWith(0, null, darwinList);
    drivelistStub.onCall(1).callsArgWith(0, null, [darwinList[0], darwinUsb]);

    driveDetector
      .on('mounted', (drive) => {
        expect(drive).to.deep.equal(darwinUsb);
        
        driveDetector.stop();
        done();
      })
      .on('error', done)
      .start(intervalTime);
  })

  it("should emit a 'unmounted' event when a drive is unmounted and another mounted", (done) => {
    drivelistStub.onCall(0).callsArgWith(0, null, darwinList);
    drivelistStub.onCall(1).callsArgWith(0, null, [darwinList[0], darwinUsb]);

    driveDetector
      .on('unmounted', (drive) => {
        expect(drive).to.deep.equal(darwinList[1]);
        
        driveDetector.stop();
        done();
      })
      .on('error', done)
      .start(intervalTime);
  })

  it("should emit a 'unmounted' event when a drive is unmounted", (done) => {
    drivelistStub.onCall(0).callsArgWith(0, null, [...darwinList, darwinUsb]);
    drivelistStub.onCall(1).callsArgWith(0, null, darwinList);

    driveDetector
      .on('unmounted', (drive) => {
        expect(drive).to.deep.equal(darwinUsb);
        
        driveDetector.stop();
        done();
      })
      .on('error', done)
      .start(intervalTime);
  })
})