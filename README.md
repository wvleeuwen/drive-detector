# Drive Detector

A module to get events when drives mount and unmount.

Usage

```javascript
import DriveDetector from 'drive-detector';

const detector = DriveDetector();

detector.on('mounted', (drive) => {
  console.log('The drive that was just now mounted:', drive);
});
  
detector.on('unmounted', (drive) => {
  console.log('The drive that was just now unmounted:', drive);
});
  
detector.on('started', (drives) => {
  console.log('Starting detector, currently these drives are mounted:', drives);
});

detector.on('error', (err) => {
  // do something with the error
  console.error(err);
});

detector.start(2000); // Default: 5000 milliseconds.
```

The DriveDetector works by a polling mechanism. In the start() method, you can define how often you want it to poll.