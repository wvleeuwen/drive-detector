import { EventEmitter } from "events";
import * as DriveList from 'drivelist';
import * as equal from 'fast-deep-equal';

export default class DriveDetector extends EventEmitter {
  private interval: any;
  private drives: Array<IDrive> = [];

  public start(intervalMilliseconds: number = 5000) {
    this.interval = setInterval(this.checkDrives.bind(this), intervalMilliseconds);
    this.checkDrives()
  }

  public stop() {
    clearInterval(this.interval);
    this.drives = [];
  }

  public list(): Promise<Array<IDrive>> {
    return new Promise((resolve, reject) => {
      DriveList.list((err, drives) => {
        if(err){
          return reject(err);
        }
        
        resolve(drives);
      })
    });
  }

  private checkDrives() {
    this.list()
      .then((detectedDrives: Array<IDrive>) => {
        if(this.drives.length === 0) {
          this.emit('started', detectedDrives);
        } else {
          this.identifyMountedDrives(this.drives, detectedDrives)
            .forEach(drive => this.emit('mounted', drive));

          this.identifyUnmountedDrives(this.drives, detectedDrives)
            .forEach(drive => this.emit('unmounted', drive));
        }
        
        this.drives = detectedDrives
      })
      .catch(err => this.emit('error', err));
  }

  private identifyMountedDrives(currentList: Array<IDrive>, newList: Array<IDrive>): Array<IDrive> {
    return newList.filter(drive => !currentList.some(currentDrive => equal(drive, currentDrive)));
  }

  private identifyUnmountedDrives(currentList: Array<IDrive>, newList: Array<IDrive>): Array<IDrive> {
    return currentList.filter(currentDrive => !newList.some(drive => equal(drive, currentDrive)));
  }
}

export interface IDrive {
  enumerator: string;
  busType: string;
  busVersion: string | null;
  device: string;
  devicePath: string;
  raw: string;
  description: string;
  error: Error | null;
  size: number;
  blockSize: number;
  logicalBlockSize: number;
  mountpoints: Array<IMountPoint>;
  isReadOnly: boolean | null;
  isSystem: boolean | null;
  isVirtual: boolean | null;
  isRemovable: boolean | null;
  isCard: boolean | null;
  isSCSI: boolean | null;
  isUSB: boolean | null;
  isUAS: boolean | null;
}

interface IMountPoint {
  path: string;
  label?: string;
}