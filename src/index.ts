import DriveDetector from "./drivedetector";

export default () => {
  return new DriveDetector();
}