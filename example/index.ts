import DriveDetector from '../src';

DriveDetector()
  .on('mounted', (drive) => {
    console.log('Drive was mounted:', drive);
  })
  .on('unmounted', (drive) => {
    console.log('Drive was unmounted:', drive);
  })
  .on('started', (drives) => {
    console.log('Starting off with list:', drives);
  })
  .on('error', console.error)
  .start(2000);